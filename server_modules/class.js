class widget{ // post it ou icone (icone soit lien soit iframe)
    constructor(id, x, y) {
        this.id = id;
        this.x = x;
        this.y = y;
    }
}

class post_it extends widget{
    constructor(id, x, y, text) {
        super(id, x, y);
        this.text = text;
    }
}

class icone extends widget{
    constructor(id, x, y,app, name) {
        super(id, x, y);
        this.app = app;
        this.name = name;
    }
}

class lien extends icone{
    constructor(id, x, y, app, name, url) {
        super(id, x, y, app, name);
        this.url = url;
    }
}

class iframe extends icone{
    constructor(id, x, y, app, name) {
        super(id, x, y, app, name);
    }
}

class calendrier extends iframe{
    constructor(id, x, y, app, name){
        super(id, x,y,app,name);
        this.eventTab = new Array();
        

    }
}

module.exports = {
    widget: widget,
    post_it: post_it,
    icone: icone,
    lien: lien,
    iframe: iframe,
    calendrier : calendrier
}