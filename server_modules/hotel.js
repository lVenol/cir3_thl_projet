class Room {
    constructor(id) {
        this.users = new Array();
        this.id = id;
    }

    isUser(socket) {
        return this.users.find(user => user.handshake.session.username == socket.handshake.session.username) != undefined;
    }

    isUserByUsername(username) {
        return this.users.find(user => user.handshake.session.username == username) != undefined;
    }

    getUser(socket) {
        return this.users.find(user => user.handshake.session.username == socket.handshake.session.username);
    }

    join(socket) {
        let user = this.getUser(socket);
        if (user != undefined) {
            clearTimeout(user.reportId);
            this.users[this.users.indexOf(user)] = socket;
        }
        else this.users.push(socket);
    }

    quit(socket) {
        let user = this.getUser(socket);
        if (user != undefined) {
            clearTimeout(user.reportId);
            this.users.splice(this.users.indexOf(user), 1);
        }
    }

    reportDisappearance(socket, callback, timeout) {
        let user = this.getUser(socket);
        if (user != undefined) user.reportId = setTimeout(() => callback(), timeout);
    }

    destroyReports() {
        this.users.forEach(user => clearTimeout(user.reportId));
    }

    packUserList() {
        return this.users.slice();
    }
}

class Hotel {
    constructor() {
        this.rooms = new Array();
    }

    isUser(socket) {
        return this.rooms.some(room => room.isUser(socket));
    }

    isUserByUsername(username) {
        return this.rooms.some(room => room.isUserByUsername(username));
    }

    getRoom(socket) {
        return this.rooms.find(room => room.isUser(socket));
    }

    getRoomById(id) {
        return this.rooms.find(room => room.id == id);
    }

    getRoomByUsername(username) {
        return this.rooms.find(room => room.isUserByUsername(username));
    }

    createRoom(id) {
        let room = this.getRoomById(id);
        if (room == undefined) room = this.rooms[this.rooms.push(new Room(id)) - 1];
        return room;
    }

    joinRoom(id, socket) {
        let room = this.getRoom(socket);

        if (room != undefined) {
            room.quit(socket);
            // Prévenir la room du départ (chat)
        }
        
        room = this.getRoomById(id);
        if (room != undefined) room.join(socket);
    }

    DestroyRoom(socket) {
        if (this.isUser(socket)) {
            let room = this.getRoom(socket);
            room.destroyReports();
            this.rooms.splice(this.rooms.indexOf(room), 1);
        }
    }

    packRoomList() {
        let list = new Array();
        this.rooms.forEach(room => list.push(room.packUserList()));
        return list;
    }
}

module.exports = Hotel;