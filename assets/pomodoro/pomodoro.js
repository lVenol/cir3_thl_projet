let tempsTravail = 25;
let tempsPause = 5;
let temps = (tempsTravail + tempsPause) * 4 * 60;
let duree = temps;
let onrun = false;

let bouton = document.getElementById('bouton');
let reset = document.getElementById('reset');

function pomodoro() {
    temps--;

    if (temps > 0) {
        setTimeout(pomodoro,1000);
    } else {
        document.getElementById('titre').innerHTML = "C'est fini !";
        onrun = false;
    }

    let secondes = (temps % 60) % 60;
    let minutes;
    
    if ((temps > (duree - tempsTravail*60) || (temps <= (duree - 60*(tempsTravail + tempsPause)) && temps > (duree - 60*(2*tempsTravail + tempsPause))) || (temps <= (duree - 60*(2*tempsTravail + 2*tempsPause)) && temps > (duree - 60*(3*tempsTravail + 2*tempsPause))) || (temps <= (duree - 60*(3*tempsTravail + 3*tempsPause))) && temps > tempsPause*60)) {
        minutes = Math.floor(temps / 60) % (tempsTravail + tempsPause) - tempsPause;
        document.getElementById('periode').innerHTML = 'Temps de travail restant :';
    } else {
        minutes = Math.floor(temps / 60) % (tempsTravail + tempsPause);
        document.getElementById('periode').innerHTML = 'Temps de pause restant :';
    }
    
    if (secondes > 9) {
        document.getElementById('timer').innerHTML = minutes + ':' + secondes;
    } else {
        document.getElementById('timer').innerHTML = minutes + ':0' + secondes;
    }
    
    if (temps == (duree - tempsTravail*60) || temps == (duree - 60*(2*tempsTravail + tempsPause)) || temps == (duree - 60*(3*tempsTravail + 10)) || temps == tempsPause*60) {
        document.getElementById('titre').innerHTML = "C'est la pause !";
    } else if (temps == (duree - 60*(tempsTravail + tempsPause)) || temps == (duree - 60*(2*tempsTravail + 2*tempsPause)) || temps == (duree - 60*(3*tempsTravail + 3*tempsPause))) {
        document.getElementById('titre').innerHTML = "Au boulot !";
    }

    if (temps == 0) {
        document.getElementById('periode').innerHTML = "--------------------";
    }
}

bouton.addEventListener('click', function() {
    if (!onrun) {
        onrun = true;
        temps = (tempsTravail + tempsPause) * 4 * 60;
        duree = temps;
        document.getElementById('titre').innerHTML = "Au boulot !";
        pomodoro();
    }
});

reset.addEventListener('click', function(){
    temps = 1;
})
