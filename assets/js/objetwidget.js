// Make the DIV element draggable:
//dragElement(document.getElementById('mydiv'));

// Déclaration d'un tableau

class obj {
  constructor(id, x, y,type) {
    this.id = id;
    this.x = x;
    this.y = y;
    this.type=type;
  }
}

function roundUp(numToRound, multiple)
{
  return multiple * Math.round(numToRound / multiple);
}

function grid(elmnt, align) {
  let parent = document.getElementById('icone');
  let top = parent.offsetTop;
  let left = parent.offsetLeft;
  let right = left + parent.offsetWidth;
  let bottom = top + parent.offsetHeight;
  let ratio = Math.max(parent.offsetWidth / 16, parent.offsetHeight / 6);
  let px = elmnt.offsetLeft < left ? 0 : Math.abs(left - elmnt.offsetLeft);
  if (align) px = roundUp(px, ratio);
  if (px + left + ratio > right) px = (Math.floor(parent.offsetWidth / ratio) - 1) * ratio;
  let py = elmnt.offsetTop < top ? 0 : Math.abs(top - elmnt.offsetTop);
  if (align) py = roundUp(py, ratio);
  if (py + top + 2 * ratio > bottom) py = (Math.floor(parent.offsetHeight / ratio) - 2) * ratio;
  px += left;
  py += top;
  elmnt.style.left = px + 'px';
  elmnt.style.top = py + 'px';
}

function dragElement(elmnt, object, callback) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    grid(elmnt, object.type == 'icone');
    let posx=elmnt.offsetLeft - pos1;// On récupère les positions
    let posy=elmnt.offsetTop - pos2;
    object.x = elmnt.offsetLeft;
    object.y = elmnt.offsetTop;
    callback('move', object);
  if (document.getElementById('header_' + elmnt.id)) {
    document.getElementById('header_' + elmnt.id).onmousedown = dragMouseDown;
  }
  else {
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e,object) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    elmnt.style.zIndex=12;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + 'px';
    posy=elmnt.offsetTop - pos2;
    elmnt.style.left = (elmnt.offsetLeft - pos1) + 'px';
    posx=elmnt.offsetLeft - pos1;// On récupère les positions
  }

  function closeDragElement() {
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
    console.log(posy);
    console.log(icone.offsetHeight-(icone.offsetHeight/9))
    if(posx<screen.width/16 && posy>icone.offsetHeight-(icone.offsetHeight/9)){
      console.log("je te détruis")
      let parentDiv = document.getElementById('icone');
      let blocDiv = document.getElementById(object.id);
      parentDiv.removeChild(blocDiv);
      callback('destroy', object);

    }
    else{
      if(object.type=='iframe'){
        elmnt.style.zIndex=4;
      }
      else if(object.type=='post_it'){
        elmnt.style.zIndex=4;
      }
      else if(object.type=='icone'){
        elmnt.style.zIndex=2;
      }
      posx = (elmnt.offsetLeft - pos1);
      posy = (elmnt.offsetTop - pos2);
      elmnt.offsetLeft = posx;
      elmnt.offsetTop = posy;
      grid(elmnt, object.type == 'icone');
      object.x = elmnt.offsetLeft;
      object.y = elmnt.offsetTop;
      callback('move', object);
    }
  }
}

function createPost_it(id, x, y, text, callback) {
  let postit = new obj(id, x, y,'post_it');

  let parent = document.getElementById('icone');
  
  let bloc = document.createElement('div');
  bloc.id = id;
  bloc.classList.add("divPostit");
  bloc.style.top = y + 'px';
  bloc.style.left = x + 'px';

  let header = document.createElement('div');
  header.setAttribute('class', 'headerPost');
  header.id = 'header_'+id;
  header.setAttribute('cursor', 'move');

  let textarea = document.createElement('textarea');
  textarea.setAttribute('class', 'postit');
  textarea.setAttribute('id', 'textarea_' + id);
  textarea.setAttribute('type','text');
  textarea.setAttribute('placeholder', 'Je suis un bloc note');
  textarea.addEventListener('keyup', () => {
    postit.text = textarea.value;
    callback('text', postit);
  });

  //bouton pour fermer le bloc note et supprime les enfants
  var close = document.createElement('button');
  close.setAttribute('class', 'closePost');
  close.setAttribute('id', 'close_' + id);
  close.setAttribute('aria-label', 'Close');
  close.addEventListener('click', () => {
    parent.removeChild(bloc);
    callback('destroy', postit);
  });


  header.appendChild(close);
  textarea.append(text);
  bloc.appendChild(header);
  bloc.appendChild(textarea);
  parent.appendChild(bloc);

  dragElement(document.getElementById(id), postit, callback);//On donne l'élément et on récupère la position finale
  document.getElementById(id).style.zIndex=4;
  return postit;
}

function createicone(id, x, y, app, name, callback){

  let icone = new obj(id, x, y,'icone');
  
  let parent=document.getElementById('icone');

  let bloc=document.createElement('div');
  bloc.style.position='absolute';
  bloc.id=id;
  bloc.style.top = y+ 'px';
  bloc.style.left=x+'px';

  let header=document.createElement('div');
  header.id='header_' + id;
  header.setAttribute('cursor','move');
  header.setAttribute('width',200);
  header.setAttribute('padding','10px');

  let image=document.createElement('img');

  let _parent = document.getElementById('icone');
  let ratio = Math.max(_parent.offsetWidth / 16, _parent.offsetHeight / 6);

  image.setAttribute('height', ratio * 0.75);
  image.setAttribute('width', ratio * 0.75);

  let titre=document.createElement('text');
  titre.setAttribute('class','titre');
  if(app=='ytb') {
    name="Youtube";
    image.setAttribute('src',"https://www.euralis.fr/wp-content/uploads/YouTube-logo-full_color.png");
  } else if (app=='ytbPlayer') {
    name="YoutubePlayer";
    image.setAttribute('src',"https://assets.stickpng.com/images/580b57fcd9996e24bc43c545.png");
  } else if (app=='spot') {
    name="Spotify";
    image.setAttribute('src',"https://i.pinimg.com/originals/0f/57/0d/0f570dcb9da53e2de2bdca3184a94549.png");
  } else if (app=='wiki') {
    name="Wikipedia";
    image.setAttribute('src',"https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Wikipedia_logo_%28svg%29.svg/1200px-Wikipedia_logo_%28svg%29.svg.png");
  } else if (app=='twit') {
    name="Twitter";
    image.setAttribute('src',"https://upload.wikimedia.org/wikipedia/fr/c/c8/Twitter_Bird.svg");
  } else if (app=='fb') {
    name="Facebook";
    image.setAttribute('src',"https://unikeo.net/blog/wp-content/uploads/2014/01/fb.png");
  } else if (app=='teams') {
    name="Teams";
    image.setAttribute('src',"https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Microsoft_Office_Teams_%282018%E2%80%93present%29.svg/1200px-Microsoft_Office_Teams_%282018%E2%80%93present%29.svg.png");
  } else if (app=='git') {
    name="Github";
    image.setAttribute('src',"https://upload.wikimedia.org/wikipedia/commons/9/91/Octicons-mark-github.svg");
  } else if (app=='pomodoro') {
    name="pomodoro";
    image.setAttribute('src', "./../img/pomodoro_app.png");
  }
  else if(app=="calcu"){
    image.setAttribute('src',"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAw1BMVEX///9mZmYAAACZmf9jY2Ofn/+cnP9bW1tfX1+dnf9gYGBpaWlcXFxYWFigoP/19fXq6ury8vLb29ubm5urq6vKysrU1NTi4uLExMS2tra8vLxwcHDX19d5eXmmpqZzc3OIiIiOju13d8aQkJA3NzcNDQ2AgIBPT09lZahGRkYeHh40NFcWFiVvb7h+ftIuLk0oKCgcHC9cXJk7OztSUokuLi4YGBiGht9ERHEQEBsvL057e808PGQLCxJZWZRKSnsjIzpOSmYFAAAPqElEQVR4nO1d6XqiyhY9YCHIEFFxTFTUOGSwO+kMdp9Op/P+T3WBqkKogUmEuudz/bjf7SQHWVbVnmvvf/654IILLrjgggsuuOCCCy64QBC0hxPHccYDu+4XOQ86s62McT/t1/06paOryQRmdb9SqWhfk/w8rCd1v1Z5GDH4+ZDqfrGy0OMQlOWnTt3vVgrcKKfvHw+P0X//F8TqcQUf9osrRVGumt9ew5+9t+t+v5PRx1z+PSjNBkRTaezwj+/qfsFT0cZMdleYXwBl8YV+0fNUpT3s3gw83HSH9v/b0cRq4pvSiKPZ/EC/UlXLUkNYlgXmm5Uzuvn/OKJDHkEPrQckUA2JAABAM1UVTHujYd0M0mCgLcog6OEP/K1JMgyZ6qalbpyuwNKog1QEm2DzN/z1LbWIMZqaqrsTUbfsDFI4NJkMGwrap0kEEUvr2hGSpAmX8IpNMFxEK5ViQHI6EW+7QgJ7zhJ6FJFOTNymR5KmuurWTSkOJEkXPIIN5WfwB2/ZGPokLbE8S2TPcAk2Wj+CP7jPzNDjqBoCeV2zJEka7NIlNE5zMPRg6oJwHGxeUhlCUbPORdDnaAiwV/tzVbo7F0Nvr05rtnb61yqQDBh6euSfw+Y+9zkMOVq9GvkNfH4erDRJo7xyLNMs0EFdW7U7tQB6CchwydWHV89QW2iBT2EFPoap6TpIpBbCWtVhAnRWIT/JeA8I/OUdxOYCfgWzwC/04PmJg/7E6W0kyzK1dJ66dFM5QcfSjy9g3CZvU7RJZdZK2H3HNVRT59ODy+hUy29gaPHPhwxeOYuIllDjPs8e9QwreS1Ntzp6/7Rdi3gZtE05zsUV8vJHiU+1x1M1iaRuVOZ0jOgtZUCNyFYYCopGvaU+uTOZWvztCtRBBey8t9iojE831sgHblKrqECLTZYzmWD2zDS5C2kl74JyMFKZn2+gkyh/LVrsFZStzB8xZ3+GT3F8Tm4BVjwnFotTz0tUjsvYVBY40ibniB4OpuRBDymeWaR2pQShjpnIz/uG0mo2my2ldTgGvfPZJZ61xP4izdl4Y92v12/GalS+GTDmfbO+W25MwlWU5YfP/X6/+/t8/Enu734ENOpTDP1lLUewLdm7ctlfK+TnyYD2rcxFkc3VI75QA9Af8F7irrUN7g4N+PmQGNwKbFGM4TwaYcUaicBtWd7VgCvDNe24VxzmS1iFUxSziGB7K3N/0BjzZKhuxj6gvaG/5FOU2A3AX+w9j6Asr04k56PHIQisFbk+nV7syz41DNHewJ36fnzkx4/9crl//V4mRZeTcTCvmWFNe+xqb/dPd9NeGWZWzxNwxhMmg1WRp4sWP0rbqFNabgcLqFbj0Ywt4wVT2UXMiYbS+It/flIAuX3NFqLmtCpbf4RLkB4XhHsW2rxPJzy+IzGFKFDPbyKGQHriq0Gb9QdEsXi4ikNQm1eYIUIh9T8LhvcZrmLRh9tsglalpVxoCb8x3WscISn4Rh3AIgj0SsN7NqTwkxfnghnml0LPbhssgtq82jIKlH7lZbZayAEtcmzYBNWqY9DQuX7g5wyK68QpS01U4GYTgA4TP/2qQCd7mv/JLkPRA7OaaFAEqAqCVyMQbtP8B7HHMNUAqL6MYAAZcgni5GRufcHyJvTrGkq1UNEqrwrCY3goxHDAIlhgq5+OVIY4pJ7vsTYjYqFtzkMhBf3UNfxWhCFDT2juWQikIrXQA6Vf17meuqL1RF0EcV0n22bzgew2M89Dx/Qe1evZoj5gzOCzxWUIg5Z5HP0hLWXqETIQMPTzyFtDLErz2MrX1CEE12d7/3QgYcrLoV/9zC1oVpQtA6RaS+kgg19shngJ59mfx9CEar0FkeiawytbYaDUQXZrsk3rCatyWzQOZJnKe4Z7gfPL2+yPoxVFlREZNvBVjj21ii10CHME2+g9qpURUj4ROOD9qsSvOjRwWDiHLqOMGVCjngiBrwLIj7+VFiIZva5ym/1RDiVHTSFufkwwF/lrd1D8K0eNyJWjHPll2uC2BCh/9BHLaj1///4r+u8cCTaX3KNanYWBMYxlLnJkgm9IMVOrLUOgu2bze8qzCnNyCS2hLuu4LIJbI4cs7JOnUBPslrI9JfndSYbnFmR+AKkpRNqjGGMQbtb129YISnKtrEbliAyuWdUXdWaB3V+93N1ZmmGgkuPMEU6yRFkXwJhho6Ma0ZfVMgrTkWbEOZri3T7CiB8nPZO46I62d3dbVTqyNGs3uPmIC32Qutm67rHY6P4FLSUwqnjVgohbJmnqYvJCSOAny6eoVlHLWRS9uIuXuBg3rGK0J90QUVMcQTgIasKfrjim3p0miMXNxoQopOf/5ZZDsGjSuCoQqpvPMFqf5XkkX1GXROhd2icY8vTasZzx53LR9NzKxmL/EP7MrfKVc4JkyPGA55jLayPSyeIQFmYLrA+zMcSBgV+HWJCuebXHFIW8Nh4g2y5FNOhSqrCSKkcMsmKQkobJEOmJZ7pWrNHCFIXVGFm0Be62wqoVayh7wRdxnIEhil1xWnXgWLlQQYwInLjVxrwUB43RR17aESX/RfUQ43Yp08JEyY4djyFqgnBKYeo5QfgWrJg+yjpy0/+4CkeIgDeNuH+os8KJMJvzxa+GQ9u05rwaD/FjyPTxYVsn3q1Wf5vCBlYV377NCDseuWbGabbJxzBsmiNMUD+GQTywy7xbCo3uHb8cDokaMYUpqQ5Z5mU6ww+BGa7isTamOoRtj/hVOA0FZlcFC+sjxPhxAlHQKv3gS5oG9IaFDEd14oKGqSyQ0faH39jpILDZRvhO7JA3Soxzy+FQ26PC9zTOillcHarsOgxIgHuHoQkvMYjZPJZIc3KyD+gmJKc+HN9hEKRPUxxEsQHg1Ht1IQVO5yNks4m5SSfxY8jNy6BQ6SerWEz5V2TniSin4HbLQEX+8o6mqPwUeQnbRDkFN1ga9lT9Ea+kajQbOJ4oZjiRiEIxnUMEnBD/dxHrZPENR76T8h01YkOYbAnrcCNj/D0oqJNF83cY9Ba0ezNZtpVY+xppof74d7f8vfzx88/xR4LGg4nEWkoWkNcl3sNaUIJkPYWW4qPf8Ai+CBqgIZ3f9KqtNmASFFMR+iDkTJZU9YzmB4T0KAKQV0GSJCnGSN/Gu4a44vKjq9DVDKfJU6CGtH15u39fv9+/3W3F1BEIpKoAbob/CLqThl8k5v+PGEXPPPTIJcwSz70hInOiagkfRJw0Y1XTkGAoWJvpGIgYWyY5Q30vprBZUcadOi2b0CAi5GI6FAE25BJmjHXGdyk7NCcEqCLtLKrCxzw9FycGyDL7zItBHt+zvuUJmJHXeTI3SXMypDkEACVmsp8nIoIsqjCl7oJkt02ILydb0XTlcMiLBHnugsQtIaFKZ+3R2HHG/Q5pl0h5rpFQaqZ4l8OSMdZDf+fpdivFnXs1TzieSHSYQmTU6AtALyDCkRfKZ4MQNem1/RWAWZ0dGeJi5TKfO8Qe12t3EYecRpf3eBnzWl5ktqpufTFh8/NhGYUWgQxBumd578yI3Rb9eojPUQwo5r66S5oL9UrTYxD3a3cI5igqh89jjb1qSLqb+6GEqkmLsp4VNqbyHLmY3mqGpdnrQlfOCH0hgTO8eVbgwN9rK5YSa4UZsbcil126hDQ168tu8xtE4BaQxa5JEB1p6ru3houzWbXLCuo4W2jeJ1EoVt8iIk3PnufSQgM/i2izDnURv/R3zwaYtf3D4nccV5Or5xUGFaerR5yi3mx7Tr0d3qdFjK4uuYj1RIZRTwFu0yu0iIXO0JS8i19LRAqqCn7pMqqYLPRuZOaxmoknJOAS8SdjoXK7YtOTqRZmWvW2W3oDwWXwB++Fnk61T6phn6JSNC7BsCi0mH9HnkTJrFyewrICTmO2AEjUFNtelDitvotZN0kbRtew4AGi+5iZFauMbuo5LNSqNARl2EgAVBvQQFYpvylyE9a9pk//4mBC9UauOniKzG5+I09YNVncpqSETdVdPWF1T8JAU/gVFA/Lk3nyqin2XpK3aQt5+idk4lnNZ+fUWWwPB/3+sHSD4MbQUZT7g9MWuQnDNfenfIpLNxAGRozLZIpnvqwlp0xB5A+wwvNklsyTqHzC356Upmb09pSAFu4Kmxx2JJVVutHXg+8Wj+BjXcBuoQntBS0aDFrv+5+L3BVWwzRQxm618VBFvIiPZxm4AjFhTVdT/VzGgMHPx8nVG+3IhDUNPZTo9BAZt3LSKQzAmhggacaQPXHMx4ni1olO/TxOV9u1IocxMviohBZrDGnjfTJ37JlcMHCCMCGmAB6HVz3u0PAqJTq8qpQUNWNyx3GmlPz849vi6uqqcdgd54IVjj6OAbVjjEji6eFzv9zvX7+OP3HLIMiYvmKEzYiely0UjG62lENY2F/o9Lcdmp+PhCFy5RBkUAzzzfHrJ5FOL/kdLbtnsaedRWaakijPYyUohmdjSVqMLXwHLK+5398kTP412P2rbsusm4wPW8N6+DdtEuMAXy4JMJwB/tBfyZfcXTrNvS475LA5biG8hMxGKNjtzjxewh7PE5ZP8ocq+qe6PYt1yit7qKqPVaj64cVv3sQlnBPK9ArD8Txx+La/gBI+052JC97W77fX5xiM68NBBhzWFMxmPT7gr1NnNnf6PZAyQB0vYFXow+nRaHD6A2/Si5LhVrTd781VemI5BXVe7V0I+9o/jOhaDT8SndgHpT0czTaaxy5l8YINqlef1fNHrCJJyo+AoTEom0HX7nTaPjod2/OSJ85qY1jZyMENWkclz0AD0BZO6GWDkl5PmurR0XRd1zTTUv3/yxwuyOPn1lTZ2l5BmzuhwwTKlzyRjYhzAFibGq9BQL+e25wgjGLeF2YIrGmtzYxWaQxbpzH01q/mZk29JH0fMPw8YZfqllv7NR1YSPec2q3nNj9DoJk9ASrnb9K0BWoTcpeXoW5dT2qv9AyA7G5eNgEnhHjj1znLZ1qr2rcnBlT5z1yrDQWJciwh0NSNEMXWCE6y2YaMtresDL3V20xEKZeHQGm9X2yCVw85NinQVd0diUXPB4rlM1Wigpr1vKcuocfOmjpiTpTAY8EYzWzCUPQ2iSHQTVXdOAMxJCcLuOqTKGs9dqvlGDQA6JqpWsZmNhL5DrwPHDP5voju1EisfSpZlu9PYKiq6v372u05/a54x46BsPpafl34ofaG3+plEU4FC0o+2/awO+iPfPQHg275KdTzIpJ4+v65PBwOy93H8UdiNgHNib7Mh5gX+nJjyJmZJWhXt0Jgd3q5FcA5KA19OmVSeqy9bvS1GL+7/xo/H+2JCzNDt9Pxf2l/khDX/LrgggsuuOCCCy644IILKsH/AHbdGcr8CmALAAAAAElFTkSuQmCC");
    name="Geogebra";
  }
  else if(app=="meteo"){
    image.setAttribute('src',"https://i.pinimg.com/originals/77/0b/80/770b805d5c99c7931366c2e84e88f251.png");
    name="Meteo";
  }
  else if(app=="googleElem"){
    image.setAttribute('src',"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTmonhWVsWat304NkUgtxc6yNFkNGE6m26BwA&usqp=CAU");
    name="Google Elements";
  }
  else{
    image.setAttribute('src',"https://assets.stickpng.com/images/5a784f86690086599e28f84c.png");
  }
  console.log(app + ' - ' + name); 
  titre.append(name);

  header.appendChild(image);
  bloc.appendChild(header);
  parent.appendChild(bloc);
  bloc.appendChild(titre);

  dragElement(document.getElementById(id),icone, callback);//On donne l'élément et on récupère la position finale
  
  return icone;
}

function createLink(id, x, y, app, name, callback, url){
    this.createicone(id, x, y, app, name, callback);

    let lien=new obj(id, x, y,'lien');

    const card = document.getElementById(id);
    if(app=="ytb"){
      url="https://www.youtube.com";
    }
    if(app=="git"){
      url="https://github.com";
    }
    if(app=="wiki"){
      url="https://fr.wikipedia.org";
    }
    if(app=="twit"){
      url="https://twitter.com";
    }
    if(app=="fb"){
      url="https://www.facebook.com";
    }
    if(app=="teams"){
      url="https://teams.microsoft.com";
    }
    if(app=="teams"){
      url="https://teams.microsoft.com";
    }
    if(app=="googleElem"){
      url="https://www.google.fr/intl/fr/sheets/about/";
    }
    card.addEventListener('dblclick', () => {
        window.open(url, '_blank');
    });
    return lien;
}

function createFrame(id, x, y, app, name, callback){
    this.createicone(id, x, y, app, name, callback);
    const card = document.getElementById(id);
    let iframe=new obj(id, x, y,'iframe');
    let id2=id+"d";
    card.addEventListener('dblclick', () => {
        if(document.getElementById(id2)==null){
            let parent=document.getElementById('icone');

            let bloc=document.createElement('div');
            bloc.style.position='absolute';
            if(app=="ytb"){
                bloc.setAttribute("width","700px");
                bloc.style.height=500;
            } else if(app=="spot"){
                bloc.style.width=300;
                bloc.style.height=80;
            }
            bloc.id=id2;
            bloc.style.top = y + 'px';
            bloc.style.left = x + 'px';


            /*let header=document.createElement('div');
            header.id='header_'+id2;
            header.setAttribute('cursor','move');
            header.setAttribute('width',700);
            header.setAttribute('padding','10px');*/

            let header = document.createElement('div');
            header.setAttribute('class', 'headerPost');
            header.id = 'header_'+id2;
            header.setAttribute('cursor', 'move');


            //bouton pour fermer la page et supprime les enfants
            let close = document.createElement('button');
            close.setAttribute('class','btn-close');
            close.setAttribute('id','btn-fermer');
            close.setAttribute('aria-label','Close');


            close.addEventListener('click',()=>{
                parent.removeChild(bloc);
            });

            let iFrame=document.createElement('iframe');
            if(app=="ytbPlayer"){
                iFrame.setAttribute('src',"https://www.youtube.com/embed/videoseries?list=PL6Xpj9I5qXYGhsvMWM53ZLfwUInzvYWsm");
                bloc.style.width=700;
                bloc.style.height=500;
            }
            else if(app=="spot"){
                iFrame.setAttribute('src',"https://open.spotify.com/embed/playlist/0jbI8OL94ETwmop098De8R" );
            }
            else if(app=="calcu"){
              iFrame.setAttribute('src',"https://www.geogebra.org/graphing?lang=fr");
            }
            else if(app=="meteo"){
              iFrame.setAttribute('src',"https://meteofrance.com/widget/prevision/593500");
            }
            else if(app=="pomodoro"){
              iFrame.setAttribute('src',"../pomodoro/pomodoro.html");
              iFrame.setAttribute("width","200px");
              iFrame.setAttribute("heigh","80px");
            }
            
            iFrame.setAttribute('class','iframe1');
            //iFrame.style.border= '25px solid red';

            //permet de détecter le click afin de faire suivre le iframe avec la div
            bloc.addEventListener('click',()=>{
            iFrame.style.width = bloc.style.width;
            iFrame.style.height = bloc.style.height;
            header.style.width=iFrame.style.width;
            callback('resize', iframe);
            iFrame.style.height = bloc.style.height - header.style.height;
            });
            header.style.width=iFrame.style.width;
            bloc.style.resize='both';//permet de changer la taille
            bloc.style.overflow='auto';
            bloc.style.overflowX='hidden';// enleve les barres
            bloc.style.overflowY='hidden';

            header.appendChild(close);
            bloc.appendChild(header);
            bloc.appendChild(iFrame);
            parent.appendChild(bloc);

            //Pour drag un élément
            dragElement(document.getElementById(id2), iframe, callback);
        }
    });
  return iframe;
}

let corbeille=document.getElementById("corbeille");
let valeur_icone;
if(screen.width/16>screen.height/9){
  valeur_icone=screen.width/16;
}
else{
  valeur_icone=screen.height/9;
}
console.log(valeur_icone)
corbeille.setAttribute("width",valeur_icone.toString()+"px");
corbeille.setAttribute("height",valeur_icone.toString()+"px");