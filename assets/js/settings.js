(function() {
    const server = io.connect('localhost:8080');
    console.log("RECHECK BIEN ton fond d'écran ( genre vraiment maintenant");
    function myownersettings(listutilisateur) {	

	    var setadminbutton= new Array(listutilisateur.length);
        var supadminbutton = new Array(listutilisateur.length);
        var supusers = new Array(listutilisateur.length);

	    let listusers = document.getElementById("listusers");
        let ulList = document.createElement("ul");
        ulList.classList.add("list-group");
        ulList.id = "ulUser";

        for(let i=0; i<listutilisateur.length;i++) {
            if(listutilisateur[i] != undefined) {
                let list = document.createElement("li");
                list.classList.add("list-group-item");
                let text = document.createTextNode(listutilisateur[i].username + " " + listutilisateur[i].grade + " ");
                list.appendChild(text);
                if(listutilisateur[i].online) {
                    let online = document.createElement("span");
                    online.id = "online";
                    online.classList.add("btn", "btn-success");
                    let onlineTxt = document.createTextNode("Online");
                    online.appendChild(onlineTxt);
                    list.appendChild(online);
                }
                else {
                    let offline = document.createElement("span");
                    offline.id = "offline";
                    offline.classList.add("btn", "btn-warning");
                    let offlineTxt = document.createTextNode("Offline");
                    offline.appendChild(offlineTxt);
                    list.appendChild(offline);
                }
                
                //listusers.innerHTML += "&nbsp";
                setadminbutton[i] = document.createElement('input');
                setadminbutton[i].type = "button";
                setadminbutton[i].id = " " +i;
                setadminbutton[i].classList.add("btn", "btn-secondary", "buttonAdmin")
                setadminbutton[i].value = "set admin";
                list.appendChild(setadminbutton[i]);

                supadminbutton[i] = document.createElement('input');
                supadminbutton[i].type = "button";
                supadminbutton[i].id = " " +i+3;
                supadminbutton[i].classList.add("btn", "btn-secondary", "buttonAdmin");
                supadminbutton[i].value = "set user";
                list.appendChild(supadminbutton[i]);

                supusers[i] = document.createElement('input');
                supusers[i].type = "button";
                supusers[i].id = i+40;
                supusers[i].classList.add("btn", "btn-secondary", "buttonAdmin")
                supusers[i].value = "supprimer";
                list.appendChild(supusers[i]);

                ulList.appendChild(list);
                listusers.appendChild(ulList);

            }
            
            if(listutilisateur[i].grade == "owner"){
                document.getElementById(supadminbutton[i].id).style.display = "none";
                document.getElementById(setadminbutton[i].id).style.display = "none";
                document.getElementById(supusers[i].id).style.display = "none";
            }
        }

        let buttonaddusers = document.createElement('input');
        buttonaddusers.type = "button";
        buttonaddusers.id = "adduserbutton";
        buttonaddusers.value = "add users";
        buttonaddusers.classList.add("btn", "btn-secondary", "buttonAdmin")
        let listBut = document.createElement("li");
        listBut.classList.add("list-group-item");
        let inputText = document.createElement("input");
        inputText.type= "text";
        inputText.id = "newUser";
        inputText.setAttribute("placeholder", "Add a new memeber");
        listBut.appendChild(inputText);
        listBut.appendChild(buttonaddusers);
        ulUser.appendChild(listBut);

        for(let i=0; i<listutilisateur.length;i++) {
            document.getElementById(setadminbutton[i].id).addEventListener('click', function() {
                if(listutilisateur[i].grade != "admin") {
                    server.emit('change_grade',listutilisateur[i].username,"admin");
                }
                else {
                }
                
            });

		    document.getElementById(supadminbutton[i].id).addEventListener('click', function() {	
                if(listutilisateur[i].grade != "user") {
                    server.emit('change_grade',listutilisateur[i].username,"user");
                }
                else {
                }
            });
            document.getElementById(supusers[i].id).addEventListener('click', function() {
                server.emit('fire',listutilisateur[i].username);
                console.log("yo");
                      }); 
                    
        }  
        document.getElementById("adduserbutton").addEventListener('click',function(){
            server.emit('username', document.getElementById("newUser").value);
        });

        document.getElementById("newUser").addEventListener('keypress', (event) =>{
            console.log("bite");
            console.log(event.key);
            if(event.key === "Enter"){
                document.getElementById('adduserbutton').click();
            }
        });
    }

    function myadminsettings(listutilisateur) {
        console.log("admin");
        var users = document.getElementById("users");
        var supusers = new Array(listutilisateur.length);

        let listusers = document.getElementById("listusers");
        let ulList = document.createElement("ul");
        ulList.classList.add("list-group");
        ulList.id = "ulUser";
        let list = document.createElement("li");
        list.classList.add("list-group-item");

        for(i=0;i<listutilisateur.length;i++) {

            if(listutilisateur[i] != undefined) {

                let list = document.createElement("li");
                list.classList.add("list-group-item");
                let text = document.createTextNode(listutilisateur[i].username + " " + listutilisateur[i].grade + " ");
                list.appendChild(text);

                if(listutilisateur[i].online) {
                    let online = document.createElement("span");
                    online.id = "online";
                    online.classList.add("btn", "btn-success");
                    let onlineTxt = document.createTextNode("Online");
                    online.appendChild(onlineTxt);
                    list.appendChild(online);
                }
                else {
                    let offline = document.createElement("span");
                    offline.id = "offline";
                    offline.classList.add("btn", "btn-warning");
                    let offlineTxt = document.createTextNode("Offline");
                    offline.appendChild(offlineTxt);
                    list.appendChild(offline);
                }
                supusers[i] = document.createElement('input');
                supusers[i].type = "button";
                supusers[i].id = i+40;
                supusers[i].classList.add("btn", "btn-secondary", "buttonAdmin")
                supusers[i].value = "supprimer";
                list.appendChild(supusers[i]);
                
                ulList.appendChild(list);
                listusers.appendChild(ulList);
            
            }
            
        }

        document.getElementById("adduserbutton").addEventListener('click',function(){
            server.emit('username', document.getElementById("newUser").value);
        });

        for(let i=0; i <listutilisateur.length;i++){
            document.getElementById(supusers[i].id).addEventListener('click', function() {
                server.emit('fire',listutilisateur[i].username);
            });
        }
        document.getElementById("adduserbutton").addEventListener('click',function(){
            server.emit('username', document.getElementById("adduser").value);
        });

        
    }       
    function myuserssettings(listutilisateur) {

        let listusers = document.getElementById("listusers");
        let ulList = document.createElement("ul");
        ulList.classList.add("list-group");
        ulList.id = "ulUser";
        let list = document.createElement("li");
        list.classList.add("list-group-item");

        for(i=0;i<listutilisateur.length;i++) {
            if(listutilisateur[i] != undefined) {

                let list = document.createElement("li");
                list.classList.add("list-group-item");
                let text = document.createTextNode(listutilisateur[i].username + " " + listutilisateur[i].grade + " ");
                list.appendChild(text);

               if(listutilisateur[i].online) {
                    let online = document.createElement("span");
                    online.id = "online";
                    online.classList.add("btn", "btn-success");
                    let onlineTxt = document.createTextNode("Online");
                    online.appendChild(onlineTxt);
                    list.appendChild(online);
                }
                else {
                    let offline = document.createElement("span");
                    offline.id = "offline";
                    offline.classList.add("btn", "btn-warning");
                    let offlineTxt = document.createTextNode("Offline");
                    offline.appendChild(offlineTxt);
                    list.appendChild(offline);
                }
                ulList.appendChild(list);
                listusers.appendChild(ulList);
            }
        }

        document.getElementById("adduserbutton").addEventListener('click',function(){
            server.emit('username', document.getElementById("newUser").value);
            // questionner le server si ce pseudo existe
        });
        document.getElementById("newUser").addEventListener('keypress', (event) =>{
            console.log("bite");
            console.log(event.key);
            if(event.key === "Enter"){
                document.getElementById('adduserbutton').click();
            }
        });


    }

    server.on('username', username => {
        if(username != undefined){
            server.emit('hire', username);   
        }
        else{
            alert("Non");
        }
    });

    server.on('settings', (grade, list) => {
        if (document.getElementById('ulUser')) document.getElementById('listusers').removeChild(document.getElementById('ulUser'));
        if (document.getElementById('search'));
        if(grade == "owner") {
            myownersettings(list);
        }

        else if(grade == "admin") {	
            myadminsettings(list);
        }

        else if(grade == "users") {
            myuserssettings(list);
        }
    });
    server.emit("grade");
    
    server.emit("grade");
    server.emit('teams');
})();