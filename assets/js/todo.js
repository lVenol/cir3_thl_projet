let __;
(function() {
  const server = io.connect('localhost:8080');

  // Create a "close" button and append it to each list item

  class todo{ // post it ou icone (icone soit lien soit iframe)
      constructor(name,isChecked,description) {
        this.name=name;
        this.isChecked=isChecked;
        this.description=description;
      }

  }
  let i;
  let tabTodo=new Array;
  // Click on a close button to hide the current list item
  let close = document.getElementsByClassName("close");


  // Create a new list item when clicking on the "Add" button
  function newElement(nom,isChecked,description,tabTodo,object) {
    let li = document.createElement("li");
    li.classList.add("todoLi");
    li.name = "todoLiName";
    let inputValue = document.getElementById("todoInput").value;
    let inputValueDesc = document.getElementById("todoDescription").value;
    let t ;
    let desc;
    if(description!=null){
      desc=description;
    }
    else{
      desc=inputValueDesc;
    }
    if(nom==null){
      t= document.createTextNode(inputValue);
    }
    else{
        t=document.createTextNode(nom);
    }
    li.appendChild(t);
    if(isChecked=='checked' && nom!=null){
          li.classList.toggle('checked');
    }
    if (inputValue === '' && nom ==null) {
      alert("You must write something!");
    } else {
      document.getElementById("todoUl").appendChild(li);
    }
    document.getElementById("todoInput").value = "";
    document.getElementById("todoDescription").value = "";

    let span = document.createElement("SPAN");
    let txt = document.createTextNode("\u00D7");
    span.className = "close";
    span.appendChild(txt);

    let br = document.createElement("br");


    let spanDesc = document.createElement("SPAN");
    let txtDesc = document.createTextNode("Description");
    spanDesc.className = "description";
    spanDesc.appendChild(txtDesc);

    li.appendChild(br);
    li.appendChild(span);
    li.appendChild(spanDesc);

    let textDesc = document.createElement("text");
    textDesc.className = "textDesc";          
    textDesc.append("Description : "+desc);
    textDesc.style.display ="none";

    li.appendChild(span);
    li.appendChild(spanDesc);
    li.appendChild(textDesc);

    let objet =new todo(t.data,isChecked,desc)


    span.onclick = function() {
      let div = this.parentElement;
      div.parentElement.removeChild(div);
      if(tabTodo.indexOf(object) == -1){
        tabTodo.splice(0, 1);
      }
      else{
        console.log(object)
        tabTodo.splice(tabTodo.indexOf(object), 1);
      }
      server.emit('todo_send', tabTodo);
      object=null;
    }
    spanDesc.onclick = function() {
        if(textDesc.style.display =="inline"){
          textDesc.style.display ="none";
        }
        else{
          textDesc.style.display ="inline";
        }
    }

    li.onclick = function() {
      if (li.name === 'todoLiName') {
        li.classList.toggle('checked');
        let indice;
        if(object!=null){
          if(tabTodo.indexOf(object) == -1){
            indice= 0;
          }
          else{
            indice=tabTodo.indexOf(object);
          }
          if(tabTodo[indice].isChecked=="checked"){
            tabTodo[indice].isChecked="";   
          }
          else{
            tabTodo[indice].isChecked="checked";
            console.log(tabTodo[indice].isChecked);
          }
        }
      }
      console.log(tabTodo)
      server.emit('todo_send', tabTodo);
    }

    //let todoUl = document.getElementById("todoUl"); 
    

    return;
    
  }
  
  // rajouter pour le changement check 
  document.getElementById("todoCreate").addEventListener('click',()=>{
    let objet = new todo(document.getElementById("todoInput").value,"",document.getElementById("todoDescription").value);
    tabTodo.push(objet);
    newElement(document.getElementById("todoInput").value,"",document.getElementById("todoDescription").value,tabTodo,objet);
    console.log(tabTodo)
    server.emit('todo_send', tabTodo);
  });

  setInterval(() => {
    if (__ != undefined) {
      init(__);
      __ = undefined;
    }
  }, 500);

  function init (data) {
    tabTodo = data;
    for (i = 0; i < close.length; i++) {
      let div = close[i].parentElement;
      div.parentElement.removeChild(div);
    }
    for(let i=0;i<data.length;i++){
      console.log(data[i])
      newElement(data[i].name,data[i].isChecked,data[i].description,tabTodo);
    }
  }

  //newElement("hit marco","checked","Une jolie description",tabTodo);
  //newElement("hit marco","","Une jolie description",tabTodo);


  //_____________________________________________________________________________
  //chat 
  //_____________________________________________________________________________
})();
