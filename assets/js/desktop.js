(function() {
    const server = io.connect('localhost:8080');

    function callback(action, object) {
        server.emit('save', action, object);
    }
    
    let tab;
    server.on('desktop', widgets => {
        tab = widgets;
        console.log(tab)
        widgets.forEach(widget => {
            if (widget.type == 'post_it') {
                console.log("ici")
                createPost_it(widget.id, widget.x, widget.y, widget.text, callback);
            }
            else if (widget.type == 'iframe') {
                createFrame(widget.id, widget.x, widget.y, widget.app, widget.name, callback);
            }
            else if (widget.type == 'lien') {
                createLink(widget.id, widget.x, widget.y, widget.app, widget.name, callback, widget.url);
            }
        });
    });

    document.getElementById("postitCreation").addEventListener("click" ,()=>{
        let obj=createPost_it(getId(), 0, 0,"", callback);
        obj.text=("");
        tab[tab.length]=obj;
        callback("create",obj);
    });

    let elemActu= new Array();
    elemActu=["calcu","meteo","spot","ytbPlayer","pomodoro"];
    for(let i=0;i<elemActu.length;i++){
        document.getElementById(elemActu[i]).addEventListener("click" ,()=>{
            let obj=createFrame(getId(), 0, 0, elemActu[i], null, callback);
            obj.type="iframe";
            obj.app=elemActu[i];
            tab[tab.length]=obj;
            callback("create",obj);
        });
    }

    let lienActu= new Array();
    lienActu=["wiki","fb","twit","ytb","git","teams","googleElem"];
    for(let i=0;i<lienActu.length;i++){
        console.log(lienActu[i])
        document.getElementById(lienActu[i]).addEventListener("click" ,()=>{
            let obj=createLink(getId(), 0, 0, lienActu[i], null, callback);
            obj.type="lien";
            obj.app=elemActu[i];
            tab[tab.length]=obj;
            callback("create",obj);
        });
    }

    //lien personnalisé
    document.getElementById("lienCrea").addEventListener("click" ,()=>{
        console.log(document.getElementById("urlCrea"))
        if(document.getElementById("urlCrea").value!="" && document.getElementById("nameCrea").value!=""){
            let obj=createLink(getId(), 0, 0, null, document.getElementById("nameCrea").value, callback,document.getElementById("urlCrea").value);
            obj.type="lien";
            obj.name=document.getElementById("nameCrea").value;
            obj.url=document.getElementById("urlCrea").value;
            obj.img="https://assets.stickpng.com/images/5a784f86690086599e28f84c.png";
            tab[tab.length]=obj;
            callback("create",obj);
        }
        else{
            alert("Veuillez remplir les deux champs")
        }
    });


    function getId() {
        let id = 0;
        tab.forEach(widget => {
            id = widget.id >= id ? widget.id + 1 : id;
        });
        console.log(id)
        return id;
    }

    //_____________________________________________________________________________
    //chat 
    server.on('chat_init', chat => {
        chat.forEach(obj => {
            if (obj.your) getvalue(obj.message.text);
            else getmessage(obj.message.text, obj.message.name, obj.message.firstname);
        });
    });

    server.on('chat_receive', (message, your) => {
        if (your) getvalue(message.text);
        else getmessage(message.text, message.name, message.firstname);
    });

    server.on('calendar_init', allEvents => {
        console.log('ok');
        _ = allEvents;
    });
    
    server.on('todo_init', data => {
        __ = data;
    });

    document.getElementById("Txt").addEventListener('keypress', (event) =>{
        console.log(event.key);
        if(event.key === "Enter"){
            document.getElementById('button-addon2').click();
        }
    });

    document.getElementById('button-addon2').addEventListener('click', () => {
        console.log(document.getElementById('Txt').value);
        let text = document.getElementById('Txt').value;
        document.getElementById('Txt').value = "";
        server.emit('chat_send', text);
    });
    
    server.on('isConnected', (isConnected, username) => {
        if (isConnected) document.getElementById("dropdownButton").innerHTML = username;
    });
    
    server.emit('ready');
})();