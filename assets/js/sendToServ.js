class event{
    constructor(date,t,c) {
      this.date = date;
      this.title=t;
      this.className =c;
    }

}

let eventTab = new Array();

let randomEvent = new event(new Date(2021, 0, 16), 'salut', 'info');
eventTab.push(randomEvent);

let randomEvent2 = new event(new Date(2021, 0, 17), 'réunion A912', 'info');
eventTab.push(randomEvent2);

let ev = new Array();

for(const elem of eventTab){
    ev.push(
      {
        title: elem.title,
        start: elem.date,
        className: elem.className
      }
    )
}