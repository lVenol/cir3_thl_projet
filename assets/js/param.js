class param{
    constructor(color,url){
      this.color = color;
      this.url = url;
    }
}


let parametre=new param("black");

function ajoutCouleur(parametre,color){

let elemDiv=document.getElementById("color");

let elemColor=document.createElement("a");
elemColor.setAttribute("class","dropdown-item");
elemColor.setAttribute("id",color);
elemColor.append(color);
elemDiv.appendChild(elemColor);

let elem=document.getElementById(color);
elem.addEventListener('click',()=>{
    parametre.color=color;
});
}

function ajoutFond(parametre,fond,url){

let elemDiv=document.getElementById("fond");

let elemFond=document.createElement("a");
elemFond.setAttribute("class","dropdown-item");
elemFond.setAttribute("id",fond);
elemFond.append(fond);
elemDiv.appendChild(elemFond);

let elem=document.getElementById(fond);
elem.addEventListener('click',()=>{
    parametre.fond=url;
});
}

ajoutCouleur(parametre,"noir");
ajoutCouleur(parametre,"bleu");
ajoutCouleur(parametre,"rouge");
ajoutCouleur(parametre,"vert");


ajoutFond(parametre,"noir","");
ajoutFond(parametre,"bleu","");
ajoutFond(parametre,"rouge","");
ajoutFond(parametre,"vert","");