let chatopen = true;
let nbvu = 0;

function showAndHideChat() {
  let div = document.getElementById("chatLobby");
  if (div.style.display === "none") {
    div.style.display = "block";
  } else {
    div.style.display = "none";
  }
}

function showAndHideTodo() {
	let div = document.getElementById("todoDiv");
	if (div.style.display == "none") {
	  div.style.display = "block";
	} else {
	  div.style.display = "none";
	}
  }

  function showAndHideCalendar() {
	let div = document.getElementById("calendarDiv");
	if (div.style.display == "none") {
	  div.style.display = "block";
	} else {
	  div.style.display = "none";
	}
  }

  function hideTCa(){
	  let todoList = document.getElementById("todoDiv");
	  let calendarlist = document.getElementById("calendarDiv");
	  todoList.style.display = "none";
	  calendarlist.style.display = "none";
  }
  
function hideChCa(){
	let chatList = document.getElementById("chatLobby");
	let calendarlist = document.getElementById("calendarDiv");
	chatList.style.display = "none";
	calendarlist.style.display = "none";
}

function hideTCh(){
	let todoList = document.getElementById("todoDiv");
	let chatList = document.getElementById("chatLobby");
	todoList.style.display = "none";
	chatList.style.display = "none";
}

function getvalue(message)
{
	username = document.createTextNode("Vous");
	let chat = document.getElementById("chatBox");
	
	//créer div global msg
	let divGlobal = document.createElement("div");
	divGlobal.classList.add("media", "w-50", "ml-auto", "mb-3");

	//créer div inter msg
	let divMsg = document.createElement("div");
	divMsg.classList.add("bg-primary", "rounded", "py-2", "px-3", "mb-2");

	let userchatTxt = document.createElement("P");
	userchatTxt.classList.add("text-small", "mb-0", "text-white");

	//creation du paragraphe avec le nom de l'utilisateur
	let user = document.createElement("P");
	user.classList.add("small", "text-muted");
	user.id = "userChat";
	user.appendChild(username);

	if(message != 0)	
	{
		userchatTxt.innerHTML += message;

		//creation du message dans la chat box
		chat.appendChild(user);
		chat.appendChild(divGlobal);
		divGlobal.appendChild(divMsg);
		divMsg.appendChild(userchatTxt)
	}	
	chat.scrollTop = chat.scrollHeight;
}


function getmessage(message,nom,prénom)
{
	let chat = document.getElementById("chatBox");

	//créer div global msg
	let divGlobal = document.createElement("div");
	divGlobal.classList.add("media", "w-50", "mb-3");

	//créer div inter msg
	let divMsg = document.createElement("div");
	divMsg.classList.add("bg-light", "rounded", "py-2", "px-3", "mb-2");

	//let otherchat = document.createElement("div");
	let otherchatTxt = document.createElement("P");
	otherchatTxt.classList.add("text-small", "mb-0", "text-muted");
	
	//otherchat.id = "otherchat";

	//creation du paragraphe avec le nom de l'utilisateur
	let user = document.createElement("P");
	user.classList.add("small", "text-muted");
	user.id = "otherChat";

	if (message != 0)
	{	
		// remplit le user 
		let text = document.createTextNode(nom + " " + prénom);
		user.appendChild(text);

		otherchatTxt.innerHTML += message;
		
		if(chatopen == true){
		
			//creation du message dans la chat box
			chat.appendChild(user);
			chat.appendChild(divGlobal);
			divGlobal.appendChild(divMsg);
			divMsg.appendChild(otherchatTxt);
			//chat.appendChild(view);


		}
	}
	chat.scrollTop = chat.scrollHeight;
	message = ""; // pas sur de ça mais bon au cas ou pour être sur que y'a pas de boucle inf
}

getmessage("Bienvenue dans le chat","","");
