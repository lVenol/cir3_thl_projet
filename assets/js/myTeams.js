(function() {
    const server = io.connect('localhost:8080');
    
    function myTeams(teams) {	
        if (document.getElementById('ulTeams')) {
            document.getElementById('ulTeams').parentElement.removeChild(document.getElementById('ulTeams'));
        }
	    let connectTeam = new Array(teams.length);

	    let listTeams = document.getElementById("mesEquipesDiv");
        let ulList = document.createElement("ul");
        ulList.classList.add("list-group");
        ulList.id = "ulTeams";

        for(let i=0; i<teams.length;i++) {
            let list = document.createElement("li");
            list.classList.add("list-group-item");
            if(teams[i] != undefined) {
                let text = document.createTextNode("Equipe : " + teams[i].name + " ");
                list.appendChild(text);
                ulList.appendChild(list);
            }
            
            connectTeam[i] = document.createElement('input');
            connectTeam[i].type = "button";
            connectTeam[i].id = " " +i;
            connectTeam[i].classList.add("btn", "btn-secondary", "buttonConnect");
            connectTeam[i].value = "Se connecter";
            list.appendChild(connectTeam[i]);

            ulList.appendChild(list);
            console.log(listTeams);
            listTeams.appendChild(ulList);

            document.getElementById(" " + i).addEventListener('click', () => {
                server.emit('switch', teams[i].name);
            });
        }
    }

    document.getElementById("createteam").addEventListener('click',function(){
        server.emit('teamname',document.getElementById("teamname").value);
    });
    server.on('teamname', teamname => {
        if(teamname !=undefined){
            server.emit('createteam', teamname);
        }
        else{
            alert("ntm");
        }
    });

    server.on('teams', teams => {
        myTeams(teams);
    });

    server.on('team_created', () => {
        window.location.href = '/desktop';
    });

    server.emit('teams');
})();