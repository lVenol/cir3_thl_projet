(function() {
 
  const server = io.connect('localhost:8080');
  
  class param{
    constructor(color){
      this.color=color;
    }
  }
  const urlParams = new URLSearchParams(window.location.search);
  const event = urlParams.get('event');
  //if (event == 'error') document.getElementById('passwordErrorLog').style.display = 'block';

  function addDesktop () {
    let newLi = document.createElement("LI");
    let newA = document.createElement("A");
    let text = document.createTextNode("Bureau");

    newA.appendChild(text);
    newA.classList.add('navbar-brand');
    newA.id = "Desktop";
    newA.href = "desktop";

    newLi.classList.add("nav-item");
    newLi.id = "desktopLi";

    let list = document.getElementById('navBar');
    list.insertBefore(newLi, list.childNodes[2]);

    let addLi = document.getElementById('desktopLi');
    addLi.insertBefore(newA, addLi.childNodes[1]);
  }

  function addMyDesktop () {
    let newLi = document.createElement("LI");
    let newA = document.createElement("A");
    let text = document.createTextNode("Mes Bureaux");

    newA.appendChild(text);
    newA.classList.add('navbar-brand');
    newA.id = "myDesktop";
    newA.href = "/desktops";
    
    newLi.classList.add("nav-item");
    newLi.id = "myDesktopLi";
    
    let list = document.getElementById('navBar');
    list.insertBefore(newLi, list.childNodes[3]);

    let addLi = document.getElementById('myDesktopLi');
    addLi.insertBefore(newA, addLi.childNodes[1]);
    
  }

  function displayLogin(isLog){
    let displayParam = document.getElementById("parametreRef");
    let displayDeco = document.getElementById("deconnexionRef");
    let displayLogin = document.getElementById("loginRef");
    let displaySign = document.getElementById("singupRef");

    if (isLog){
      displayParam.setAttribute("style", "display: block;");
      displayDeco.setAttribute("style", "display: block;");
      displayLogin.setAttribute("style", "display: none;");
      displaySign.setAttribute("style", "display: none;");
      console.log("ok");
    }
    else{
      displayParam.setAttribute("style", "display: none;");
      displayDeco.setAttribute("style", "display: none;");
      displayLogin.setAttribute("style", "display: block;");
      displaySign.setAttribute("style", "display: block;");
    }
  }

  server.on('isConnected', (isConnected, username) => {
    if (isConnected) {

      document.getElementById("dropdownButton").innerHTML = username;

      addDesktop();
      addMyDesktop();
    }
    displayLogin(isConnected);
  });
  server.emit('isConnected');
})();
