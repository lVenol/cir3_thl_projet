const express = require('express');
const session = require('express-session')({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
});
const app = express();
const path = require('path');
const bodyparser = require('body-parser');
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const sharedsession = require('express-socket.io-session');
const Hotel = require('./server_modules/hotel.js');
const Class = require('./server_modules/class.js');

const MongoClient = require('mongodb').MongoClient;
const MongoObjectID = require("mongodb").ObjectID;
const url = 'mongodb://administrator:123456@grushaisen.ddns.net:27017/';
const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    serverSelectionTimeoutMS: 5000,
    connectTimeoutMS: 1000,
    socketTimeoutMS: 1000
};

app.use(session);
io.use(sharedsession(session));

app.use(bodyparser.urlencoded({extended : true}));
app.use(bodyparser.json());

app.use(express.static(path.join(__dirname, '/assets')));

let hotel = new Hotel();

app.get('/', (req, res) => {
	res.sendFile(__dirname + '/assets/views/Accueil.html')
});

app.get('/login', (req, res) => {
	res.sendFile(__dirname + '/assets/views/Login.html')
});

app.post('/login', (req, res) => {
	let username = req.body.username;
	let password = req.body.password;
	
	MongoClient.connect(url, options, function (err, client) {
		if (err) console.log('Impossible de connecter à GrushaV1');
		let db = client.db('GrushaV1');

		db.collection('users').findOne({ username:username }, function(error, result) {
			if (error) throw error;

			if (result) {
				if (result.password == password) {
					req.session.loggedin = true;
					req.session.username = username;
					req.session.firstname = result.firstname;
					req.session.lastname = result.lastname;
					req.session.lastId = result.lastId;
					req.session.admin = true;

					console.log(username + ' connected with password ' + password);
					console.log('ID : ' + req.session.lastId);

					res.redirect('/desktop');
				}
				else res.redirect('login/?event=error');
			}
			else res.redirect('login/?event=error');
			
			client.close();
		});
	});
});

app.get('/signup', (req, res) => {
	res.sendFile(__dirname + '/assets/views/SignUp.html')
});

app.post('/sign_up', (req, res) => {


	let username = req.body.username;
	let firstname = req.body.firstname;
	let lastname = req.body.lastname;
	let password = req.body.password;
	let confirm = req.body.confirm;
	
	if(username == "" || firstname == "" || lastname=="" || password=="" || password != confirm){
		//console.log("empty field");
		//alert("Champs manquants")
		res.redirect('login/?event=error');
		return;
	}


	let widgets = new Array();
	let widgetsBdd = new Array();

    widgets.push(new Class.post_it(13, 600, 300, ''));
    
	widgets.push(new Class.lien(1, 0, 110, 'wiki'));
	widgets.push(new Class.lien(2, 0, 250, 'fb'));
	widgets.push(new Class.lien(3, 0, 387, 'twit'));
	widgets.push(new Class.lien(4, 0, 525, 'git'));
	widgets.push(new Class.lien(5, 0, 663, 'googleElem'));
    widgets.push(new Class.lien(6, 138, 110, 'ytb'));
    widgets.push(new Class.lien(7, 138, 250, 'teams'));

	widgets.push(new Class.iframe(8, 138, 387, 'meteo'));
	widgets.push(new Class.iframe(9, 138, 525, 'pomodoro'));

	widgets.push(new Class.iframe(10, 277, 110, 'ytbPlayer'));
	widgets.push(new Class.iframe(11, 277, 250, 'spot'));
	widgets.push(new Class.iframe(12, 277, 387, 'calcu'));


	for(const element of widgets){
		widgetsBdd.push(translate(element)); 
	}

	MongoClient.connect(url, options, function (err, client) {
		if (err) console.log('Impossible de connecter à GrushaV1');
		let db = client.db('GrushaV1');

		let users = new Array();
		users.push({
			username: username,
			grade: 'owner'
		});

		let team = {
			name: username + '\'s private saloon',
			users: users,
			messages: new Array(),
			events: new Array(),
			todo: new Array(),
		
		}
		db.collection('teams').insertOne(team, null, function (error, results) {
			if (error) throw error;	

			let user = {
				username: username,
				firstname: firstname,
				lastname: lastname,
				password: password,
				lastId: results.insertedId,
				widgets: widgetsBdd
			};
			db.collection('users').insertOne(user, null, function (error) {
				if (error) throw error;
	
				req.session.loggedin = true;
				req.session.username = username;
				req.session.firstname = firstname;
				req.session.lastname = lastname;
				req.session.admin = true;
				req.session.lastId = results.insertedId;
				
				console.log(username + 'connected with password ' + password);
				console.log('ID : ' + req.session.lastId);
				
				res.redirect('/desktop');
			
				client.close();
			});
		});
	});
});

app.get('/desktop', (req, res) => {
	if (req.session.loggedin) res.sendFile(__dirname + '/assets/views/Dekstop.html');
	else res.redirect('/');
});

app.get('/desktops', (req, res) => {
	if (req.session.loggedin) res.sendFile(__dirname + '/assets/views/MesBureau.html');
	else res.redirect('/');
});

app.get('/info', (req, res) => {
	res.sendFile(__dirname + '/assets/views/info.html')
});

app.get('/settings', (req, res) => {
	if (req.session.loggedin) res.sendFile(__dirname + '/assets/views/settings.html');
	else res.redirect('/');
});

app.get('/disconnect', (req, res) => {
	req.session.loggedin = false;
	res.redirect('/');
});

io.on('connection', socket => {
	socket.on('isConnected', () => socket.emit('isConnected', socket.handshake.session.loggedin ? true : false, socket.handshake.session.username));
	socket.on('exists', username => {
		MongoClient.connect(url, options, function (err, client) {
			if (err) console.log('Impossible de connecter à GrushaV1');
			let db = client.db('GrushaV1');
			
			db.collection('users').findOne({ username: username }, function(error, result) {
				if (error) throw error;
				console.log('veut ajouter ' + username);
				if (result) {
					socket.emit('exists', username);
				}
				else {
					socket.emit('exists', undefined);
				}
			});
		});
	});
	socket.on('teams', () => {
		console.log('need teams');
		MongoClient.connect(url, options, function (err, client) {
			if (err) console.log('Impossible de connecter à GrushaV1');
			let db = client.db('GrushaV1');
			
			db.collection('teams').find({}).toArray(function(error, result) {
				if (error) throw error;
				result = result.filter(team => {
					let users = team.users;
					return users.find(user => user.username == socket.handshake.session.username) != undefined ? true : false;
				});
				socket.emit('teams', result);
			});
		});
	});
	socket.on('switch', name => {
		MongoClient.connect(url, options, function (err, client) {
			if (err) console.log('Impossible de connecter à GrushaV1');
			let db = client.db('GrushaV1');
			
			db.collection('teams').findOne({ name: name }, function(error, result) {
				if (error) throw error;
				if (result) {
					db.collection('users').updateOne(
						{ username: socket.handshake.session.username }, 
						{
							$set: {
								lastId: result._id.toString()
							},
						},
						null,
						() => {
							socket.emit('team_created');
							socket.handshake.session.lastId = result._id.toString();
							client.close();
						}
					);
				}
			});
		});
	});
	socket.on('username', username => {
		MongoClient.connect(url, options, function (err, client) {
			if (err) console.log('Impossible de connecter à GrushaV1');
			let db = client.db('GrushaV1');
			
			db.collection('users').findOne({ username: username }, function(error, result) {
				if (error) throw error;
				console.log('veut ajouter ' + username);
				if (result) {
					db.collection('teams').findOne({ _id: new MongoObjectID(socket.handshake.session.lastId) }, function(error, _result) {
						if (error) throw error;
						
						if (_result) {
							if (_result.users.find(user => user.username == username) == undefined) {
								socket.emit('username', username);
							}
							else{
								socket.emit('username', undefined);
							}
						}
					});
				}
				else {
					socket.emit('username', undefined);
					console.log('n\'existe pas');
				}
			});
		});
	});
	function sendgrade() {
		MongoClient.connect(url, options, function (err, client) {
			if (err) console.log('Impossible de connecter à GrushaV1');
			let db = client.db('GrushaV1');
			
			db.collection('teams').findOne({ _id: new MongoObjectID(socket.handshake.session.lastId) }, function(error, result) {
				if (error) throw error;

				if (result) {
					let users = new Array();
					let _room = hotel.packRoomList().find(room => room.id == socket.handshake.session.lastId);
					if (_room != undefined) {
						users = _room.packUserList();
					}
					result.users.map(user => {
						user.online = users.some(_user => _user.username == user.username);
						return user;
					});
					console.log(socket.handshake.session.username);
					console.log(result.users);
					if (result.users.find(user => user.username == socket.handshake.session.username)) {
						socket.emit('settings', result.users.find(user => user.username == socket.handshake.session.username).grade, result.users);
					}
				}
			});
		});
	}
	socket.on('grade', () => {
		if (socket.handshake.session.loggedin) {
			sendgrade();
		}
	});
	let fire = false;
	socket.on('fire', username => {
		if (!fire) {
			fire = true;
			MongoClient.connect(url, options, function (err, client) {
			if (err) console.log('Impossible de connecter à GrushaV1');
			let db = client.db('GrushaV1');
			
			db.collection('teams').findOne({ _id: new MongoObjectID(socket.handshake.session.lastId) }, function(error, result) {
				if (error) throw error;
				
				if (result) {
					result.users.splice(result.users.indexOf(result.users.find(user => user.username == username)), 1);

					db.collection('teams').updateOne(
						{ _id: new MongoObjectID(socket.handshake.session.lastId) }, 
						{
							$set: {
								users: result.users
							},
						},
						null,
						() => {
					
							sendgrade();
							fire = false;
						}
					);
				}
			});
		});
		}
	});
	let hire = false;
	socket.on('hire', username => {
		if (!hire) {
			hire = true;
			MongoClient.connect(url, options, function (err, client) {
			if (err) console.log('Impossible de connecter à GrushaV1');
			let db = client.db('GrushaV1');
			
			db.collection('teams').findOne({ _id: new MongoObjectID(socket.handshake.session.lastId) }, function(error, result) {
				if (error) throw error;
				
				if (result) {
					result.users.push({username: username, grade: 'user'});
					
					db.collection('teams').updateOne(
						{ _id: new MongoObjectID(socket.handshake.session.lastId) }, 
						{
							$set: {
								users: result.users
							},
						},
						null,
						() => {
							sendgrade();
							hire = false;
						}
					);
				}
			});
		});
		}
	});
	let change_grad = false;
	socket.on('change_grade', (username, grade) => {
		if (!change_grad) {
			change_grad = true;
			MongoClient.connect(url, options, function (err, client) {
				if (err) console.log('Impossible de connecter à GrushaV1');
				let db = client.db('GrushaV1');
				
				db.collection('teams').findOne({ _id: new MongoObjectID(socket.handshake.session.lastId) }, function(error, result) {
					if (error) throw error;
					
					if (result) {
						let user = result.users.find(_user => _user.username == username);
						if (user) {
							user.grade = grade;
							db.collection('teams').updateOne(
								{ _id: new MongoObjectID(socket.handshake.session.lastId) }, 
								{
									$set: {
										users: result.users
									},
								},
								null,
								() => {
									sendgrade();
									change_grad = false;
								}
							);
						}
					}
				});
			});	
		}
	});
	socket.on('teamname', name => {
		MongoClient.connect(url, options, function (err, client) {
			if (err) console.log('Impossible de connecter à GrushaV1');
			let db = client.db('GrushaV1');

			db.collection('teams').findOne({ name: name }, function(error, result) {
				if (error) throw error;
				
				socket.emit('teamname', result ? undefined : name);
			});
		});	
	});
	socket.on('createteam', name => {
		MongoClient.connect(url, options, function (err, client) {
			if (err) console.log('Impossible de connecter à GrushaV1');
			let db = client.db('GrushaV1');
	
			let users = new Array();
			users.push({
				username: socket.handshake.session.username,
				grade: 'owner'
			});
			let team = {
				name: name,
				users: users,
				messages: new Array(),
				events: new Array(),
				todo: new Array()
			}
			db.collection('teams').insertOne(team, null, function (error, results) {
				if (error) throw error;	

				db.collection('users').updateOne(
					{ username: socket.handshake.session.username }, 
					{
						$set: {
							lastId: results.insertedId
						},
					},
					null,
					() => {
						socket.emit('team_created');
						client.close();
					});
				});
			});
	});
	let room;
	socket.on('ready', () => {
		if (socket.handshake.session.loggedin) {
			room = hotel.createRoom(socket.handshake.session.lastId);
			hotel.joinRoom(socket.handshake.session.lastId, socket);
			console.log(socket.handshake.session.username + ' joined ' + socket.handshake.session.lastId)

			MongoClient.connect(url, options, function (err, client) {
				if (err) console.log('Impossible de connecter à GrushaV1');
				let db = client.db('GrushaV1');
				
				db.collection('teams').findOne({ _id: new MongoObjectID(socket.handshake.session.lastId) }, function(error, result) {
					if (error) throw error;

					if (result) {
						socket.emit('chat_init', result.messages.map(message => {
							let obj = new Object();
							obj.message = message;
							obj.your = message.firstname == socket.handshake.session.username;
							return obj;
						}));
						socket.emit('calendar_init', result.events);
						socket.emit('todo_init', result.todo);
					}
				});

				db.collection('users').findOne({ username:socket.handshake.session.username }, function(error, result) {
					if (error) throw error;

                    if (result) socket.emit('desktop', result.widgets);
				});
			});
		}
	});
	socket.on('save', (action, object) => {
		if (socket.handshake.session.loggedin) {
			MongoClient.connect(url, options, function (err, client) {
				if (err) console.log('Impossible de connecter à GrushaV1');
				let db = client.db('GrushaV1');
	
				db.collection('users').findOne({ username:socket.handshake.session.username }, function(error, result) {
					if (action == 'move') {
						let widget = result.widgets.find(_widget => _widget.id == object.id);
						if (widget == undefined) return;
	
						widget.x = object.x;
						widget.y = object.y;
	
						db.collection('users').updateOne(
							{ username: socket.handshake.session.username }, 
							{
								$set: {
									widgets: result.widgets
								},
							}
						);
					}
					else if (action == 'text') {
						let widget = result.widgets.find(_widget => _widget.id == object.id);
						if (widget == undefined) return;
	
						widget.text = object.text;
	
						db.collection('users').updateOne(
							{ username: socket.handshake.session.username }, 
							{
								$set: {
									widgets: result.widgets
								},
							}
						);
					}
					else if (action == 'destroy') {
						let widget = result.widgets.find(_widget => _widget.id == object.id);
						if (widget == undefined) return;
	
						result.widgets.splice(result.widgets.indexOf(widget), 1);
	
						db.collection('users').updateOne(
							{ username: socket.handshake.session.username }, 
							{
								$set: {
									widgets: result.widgets
								},
							}
						);
					}
					else if (action == 'create') {
						result.widgets.push(object);
	
						db.collection('users').updateOne(
							{ username: socket.handshake.session.username }, 
							{
								$set: {
									widgets: result.widgets
								},
							}
						);
					}
				});
			});
		}
	});
	socket.on('chat_send', text => {
		if (socket.handshake.session.loggedin) {
			MongoClient.connect(url, options, function (err, client) {
				if (err) console.log('Impossible de connecter à GrushaV1');
				let db = client.db('GrushaV1');
				
				db.collection('teams').updateOne(
					{ _id: new MongoObjectID(socket.handshake.session.lastId) }, 
					{
						$push: {
							messages: { text: text, firstname: socket.handshake.session.username, lastname: '' }
						},
					}
				);
			});
			room.packUserList().forEach(user => {
				user.emit('chat_receive', {
					text: text,
					name: socket.handshake.session.username,
					firstname: ''
				}, user.handshake.session.username == socket.handshake.session.username);
			});
		}
	});
	socket.on('calendar_send', events => {
		if (socket.handshake.session.loggedin) {
			MongoClient.connect(url, options, function (err, client) {
				if (err) console.log('Impossible de connecter à GrushaV1');
				let db = client.db('GrushaV1');
				
				db.collection('teams').updateOne(
					{ _id: new MongoObjectID(socket.handshake.session.lastId) }, 
					{
						$set: {
							events: events
						},
					}
				);
			});
			if (room != undefined) {
				room.packUserList().forEach(user => {
					user.emit('calendar_receive', events);
				});
			}
		}
	});
	socket.on('todo_send', todo => {
		if (socket.handshake.session.loggedin) {
			MongoClient.connect(url, options, function (err, client) {
				if (err) console.log('Impossible de connecter à GrushaV1');
				let db = client.db('GrushaV1');
				
				db.collection('teams').updateOne(
					{ _id: new MongoObjectID(socket.handshake.session.lastId) }, 
					{
						$set: {
							todo: todo
						},
					}
				);
			});
			console.log(room);
			if (room != undefined) {
				room.packUserList().forEach(user => {
					user.emit('todo_init', todo);
				});
			}
		}
	});
});

server.listen(8080);
console.log('Server connected');

//_____________________________________________________________________________
//_____________________________________________________________________________

function translate(widget) {

    let object = new Object();
    
    object.x = widget.x;
	object.y = widget.y;
	object.id = widget.id;

    if (widget instanceof Class.post_it) {
        object.text = widget.text;
        object.type = 'post_it';
	}
	else {
        object.img = widget.img;
        object.name = widget.name;
        object.app = widget.app
        object.url = widget.url;
		object.type = widget instanceof Class.iframe ? 'iframe' : 'lien';
	}

	return object;
}