// Make the DIV element draggable:
//dragElement(document.getElementById("mydiv"));

// Déclaration d'un tableau

var tabWidget= new Array;

function dragElement(elmnt,posx,posy) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    // if present, the header is where you move the DIV from:
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    elmnt.style.zIndex=12;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    posy=elmnt.offsetTop - pos2;
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    posx=elmnt.offsetLeft - pos1;// On récupère les positions
  }

  function closeDragElement() {
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
    for(var i= 0; i < tabWidget.length; i++){
      document.getElementById(tabWidget[i].nomid).style.zIndex=1;
    }
    elmnt.style.zIndex=2;
  }
  //return posx et posy ?
}


class widget{ // post it ou icone (icone soit lien soit iframe)
    constructor(tip) {
        this.type=tip
    }

}

class post_it extends widget{
    constructor(texte,x,y,id,idb) {
        super();
        this.nomid=id;
        this.texteNote=texte;
        this.posx=x;
        this.posy=y;
        this.nomidbloc=idb;
    }
    createPost_it(){
      var elem=document.getElementById("icone");
      var elem2=document.createElement("div");
      elem2.style.position="absolute";
      elem2.id=this.nomid;
       elem2.style.top = this.posy+ "px";
       elem2.style.left=this.posx+"px";
       var elem4=document.createElement("div");
       elem4.setAttribute("class","elembloc");
       elem4.id=this.nomid+"header";
       elem4.setAttribute("cursor","move");

       var elem3=document.createElement("textarea");
       elem3.setAttribute("class","postit");
       elem3.setAttribute("id",this.nomidbloc);
       elem3.setAttribute("type","text");

       //bouton pour fermer le bloc note et supprime les enfants
       var elembut =document.createElement("button");
       elembut.setAttribute("class","close");
       elembut.setAttribute("id","btn-fermer");
       elembut.setAttribute("aria-label","Close");
       elembut.addEventListener('click',()=>{
        elem.removeChild(elem2);
       });


       elem4.appendChild(elembut);
       elem3.append(this.texteNote);
       elem2.appendChild(elem4);
       elem2.appendChild(elem3);
       elem.appendChild(elem2);
       dragElement(document.getElementById(this.nomid,this.posx,this.posy));//On donne l'élément et on récupère la position finale

       elem3.addEventListener('dblclick', () => {
        
      });

    }
}

class icone extends widget{
    constructor(x,y,image,nom,nomapp) {
        super();
        this.posx=x;
        this.posy=y;
        this.imageActu=image;
        this.nomid=nom;
        this.urlSitenomApp=nomapp;
    }
    createicone(h,w){
      //création de l'icone déplacable
       var elem=document.getElementById("icone");
       var elem2=document.createElement("div");
       elem2.style.position="absolute";
       elem2.style.width=w+50;
       elem2.id=this.nomid;
       elem2.style.top = this.posy+ "px";
       elem2.style.left=this.posx+"px";
       var elem4=document.createElement("div");
       elem4.id=this.nomid+"header";
       elem4.setAttribute("cursor","move");
       elem4.setAttribute("width",w);
       elem4.setAttribute("padding","10px");
       var elem3=document.createElement("img");
       elem3.setAttribute("src",this.imageActu);
       elem3.setAttribute("height",h);
       elem3.setAttribute("width",w);
       var elemtext=document.createElement("text");
       elemtext.append(this.nomApp);
       elem4.appendChild(elem3);
       elem2.appendChild(elem4);
       elem.appendChild(elem2);
       elem2.appendChild(elemtext);
       dragElement(document.getElementById(this.nomid,this.posx,this.posy));//On donne l'élément et on récupère la position finale
    }
}

class lien extends icone{
    constructor(url,x,y,image,nom,nomapp) {
        super(x,y,image,nom,nomapp);
        this.urlSite=url;
    }

    createLink(h,w){
      this.createicone(h,w);
      const card = document.getElementById(this.nomid);
      card.addEventListener('dblclick', () => {
        window.open(this.urlSite, '_blank');
      });
    }
}

class iframe extends icone{
    constructor(url,x,y,image,nom,ndiv,nomapp) {
        super(x,y,image,nom,nomapp);
        this.posx=x;
        this.posy=y;
        this.urlSite=url;
        this.nomdiv=ndiv;
    }
    createFrame(h,w){
      this.createicone(h,w);
      const card = document.getElementById(this.nomid);
      card.addEventListener('dblclick', () => {
        var elem=document.getElementById("icone");
        var elembr=document.createElement("br");
        elem.appendChild(elembr);
        var elem2=document.createElement("div");
       elem2.style.position="absolute";
       elem2.style.width=w+100;
       elem2.id=this.nomdiv;
       elem2.style.top = this.posy+ "px";
       elem2.style.left=this.posx+"px";
       var elem4=document.createElement("div");
       elem4.id=this.nomdiv+"header";
       elem4.setAttribute("cursor","move");
       elem4.setAttribute("width",w);
       elem4.setAttribute("padding","10px");


       //bouton pour fermer la page et supprime les enfants
       var elembut =document.createElement("button");
       elembut.setAttribute("class","btn-close");
       elembut.setAttribute("id","btn-fermer");
       elembut.setAttribute("aria-label","Close");
       elembut.addEventListener('click',()=>{
        elem.removeChild(elem2);
       });

       //<button type="button" class="btn btn-dark">Dark</button>
       //bouton pour aggrandir


       var elem3=document.createElement("iframe");
       elem3.setAttribute("src",this.urlSite);
       elem3.setAttribute("class","iframe1");
       elem4.appendChild(elembut);
       elem4.appendChild(elem3);
       elem2.appendChild(elem4);
       elem.appendChild(elem2);
       dragElement(document.getElementById(this.nomdiv,0,0));
      });
    }
}

ico = new icone(150,350,"https://cde.peru.com//ima/0/1/6/9/6/1696691/924x530/videos.jpg","base","ytb");
ico.createicone(400,400);

ico2 = new iframe("https://www.youtube.com/embed/videoseries?list=PL6Xpj9I5qXYGhsvMWM53ZLfwUInzvYWsm",0,0,"https://img2.freepng.fr/20180330/iyw/kisspng-youtube-red-logo-computer-icons-youtube-5abe39fec046a9.7547336915224161267876.jpg","base02","id2","ytb");
ico2.createFrame(400,500);

ico3 = new iframe("../assets/calendar/fullcalendar.html",0,0,"https://png.pngtree.com/png-vector/20190216/ourmid/pngtree-vector-calendar-icon-png-image_540870.jpg","base03","id3","ytb");
ico3.createFrame(100, 200);
console.log(ico3);

postit = new post_it("Je suis un bloc note",0,0,"aert","4");
postit.createPost_it();

tabWidget.push(ico);
tabWidget.push(ico2);
tabWidget.push(ico3);
tabWidget.push(postit);