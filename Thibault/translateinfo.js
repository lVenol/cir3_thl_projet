function translateAndSend(myWidget){

    let object = new Object();
    
    object.posX = myWidget.posX;
    object.posY = myWidget.posY;

    if(myWidget instanceof post_it){
        //postit
        object.nomid = myWidget.nomid;
        object.texteNote = myWidget.texteNote;
        object.nomidbloc = myWidget.nomidbloc;
        object.type = "post_it";
    }else{
        //icone
        object.imageActu = myWidget.imageActu;
        object.nomid = myWidget.nomid;
        object.urlSitenomApp = myWidget.urlSitenomApp;
        object.urlSite = myWidget.urlSite;

        if(myWidget instanceof iframe){
            //iframe
            object.nomdiv = myWidget.nomdiv;
            object.type = "iframe";
        } 
        else object.type = "lien";
    }

    return object;

}

