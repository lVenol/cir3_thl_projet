var MongoClient = require("mongodb").MongoClient;

console.log("_______________ New Entry _______________");

//************************************************************************************** */
MongoClient.connect('mongodb://localhost:27017', {useUnifiedTopology: true}, function (err, client) {
    if (err) throw err;
    else console.log("successfully connected to the database");

    let db = client.db('new_york');

    //************************************************************************************** */
    db.collection("restaurants").findOne({name:"Riviera Caterer"}, function(error, result) {
        if (error) throw error;
        console.log(
            "Name : " + result.name + "\n",
            "ID : " + result._id 
        );
    });
    //************************************************************************************** */
    db.collection("restaurants").find().toArray(function (error, myTab) {
        if (error) throw error;
        console.log("There are " + myTab.length + " restaurant in New York")
    });

    //************************************************************************************** */
    var objNew = { name: "Au Vintage", borough:"Douai"};  

    db.collection("restaurants").insert(objNew, null, function (error, results) {
        if (error) throw error;
        console.log("Resto inserted");    
    });
    //************************************************************************************** */
    db.collection("restaurants").findOne({name:"Au Vintage"}, function(error, result) {
        if (error) throw error;
        console.log(
            "Name : " + result.name + "\n",
            "ID : " + result._id 
        );
    });
});

console.log("_______________ The End   _______________")
